import { ref } from 'vue'
import { defineStore } from 'pinia'
import type { Tag } from '@/types/Tag';
import tagService from "@/service/tag";


export const useTagStore = defineStore("tag", () => {
  const tag = ref<Tag>();
  const tags = ref<Tag[]>([]);
  async function getTags() {
    try {
      const res = await tagService.getTags();
      tags.value = res.data; 
    } catch (error) {
      console.log(error)
    }
  }
  async function  getTagById(id : number) {
    try {
      const res = await tagService.getTagById(id);
      tag.value = res.data
    } catch (error) {
      console.log(error)
    }
  }



  return { getTagById, tag , tags , getTags};
});


import { ref, type Ref } from 'vue'
import router from '@/router'
import { defineStore } from 'pinia'
import type { Post } from '@/types/Post';
import posttag from '@/service/posttag';
import { useUserStore } from './user';
import moment from 'moment-timezone';
import postService from '@/service/post';

export const usePostStore = defineStore("post", () => {
  const userStore = useUserStore();
  const posts = ref<Post[]>([]);
  const post = ref<Post>();
  const diarys = ref<Post[]>([]);
  const diary = ref<Post>();
  
  
  async function getPostAll() {
      try {
        const res = await postService.getPostAll();
        posts.value = res.data;
      } catch (error) {
        console.error('Error get AllPosts:', error)
      }
  }

  async function getPosts(user_id: number) {
    try {
        const res = await postService.getPosts(user_id);
        posts.value = res.data;
    } catch (error: any) {
        if (error.response && error.response.status === 403) {
            window.location.href = '/forbidding';
        } else {
            console.error('Error getPosts', error);
        }
    }
}


  const removePostById = (postId:number) => {
    posts.value = posts.value.filter(post => post.id !== postId);
  }
  

  async function getPostById(id : number) {
      try {
        
        const res = await postService.getPostById(id)
        post.value = res.data;
      } catch (error) {
        console.error("Error getPostById : " , error)
      }
  }

  function postPostTag(page : number ,tag_ : number) {
    try{
      if (userStore.currentUser.role === "personnel" || userStore.currentUser.role === "researcher"){
        const timestamp = new Date("2024-02-14 12:00:00.000") 
        const user_ = userStore.currentUser.id
        posttag.posttags(timestamp , user_ ,page ,tag_)
        
      }
      else {
        throw new Error("User does not have the required role");
        router.push('/login');
      }

      

    } catch(error) {
      console.error('Error post PostTag :', error);
    }
  }

  function postDiary(content: string , user_id: number) {
    try {
      const source = "Diary"
      const datetime = moment.tz('Asia/Bangkok').format('YYYY-MM-DD HH:mm:ss');
      postService.postDiary(content , source , datetime , user_id)
    } catch (error) {
      console.error('Error postDiary')
    }
  }

  function editDiary(id: number ,  txtContent: string){
    try {
      postService.patchDiary(id , txtContent);
    } catch (error) {
      console.error('Error editDiary')
    }
  }

 

  async function getPostBySource(user_id: number, searchText?: string, searchDate?: string | null) {
      try {
        const res = await postService.getPostBySource(user_id, searchText , searchDate )
        diarys.value = res.data;
    } catch (error) {
        console.error('Error getting PostBySource:', error);
        throw error;
    }
  }
  

  async function getPostBySourceId(user_id: number ,id : number) {
    try {
      const res = await postService.getPostBySourceId(user_id , id)
      diary.value = res.data
    } catch (error) {
      console.error('Error get PostBySourceID')
      throw error;
    }
  }

  

  async function getPostBySearch(searchText: string) {
    try {
      const response = await postService.getPostBySearch(searchText)
      posts.value = response.data
    } catch (error) {
      console.error('Error get PostBySourceID')
      throw error;
    }
  }

  async function deletePostById(id: number) {
    try {
      const response = await postService.deletePostById(id);
      post.value = response.data;
    } catch (error) {
      console.error(`Error fetching post with id ${id}:`, error);
      throw error;
    }
  }

  return {posts,removePostById, getPosts, getPostById, post, postPostTag , postDiary ,editDiary, getPostBySource , getPostBySourceId , diarys , diary,getPostAll,deletePostById,getPostBySearch};
});


import type { User } from '@/types/User';
import { defineStore } from "pinia";
import { ref } from "vue";

export const useUserStore = defineStore("userStore", () => {
    const currentUser = ref<User>({ id: -1, role: '', email: '', password: '', username: '' })

    function setUser(user: User) {
        currentUser.value = { ...user };
        localStorage.setItem('user', JSON.stringify(user));
        return currentUser.value;
    }

    function getUser(user: User) {
        localStorage.getItem('user');
        return currentUser.value;
    }

    return { currentUser, setUser, getUser };
});

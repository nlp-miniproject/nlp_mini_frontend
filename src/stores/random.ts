import { defineStore } from 'pinia';
import { usePostStore } from './post';
import { useRouter } from 'vue-router';
import { ref } from 'vue';

export const useRandomStore = defineStore('random', () => {
  const postStore = usePostStore();
  const router = useRouter();
  const randomizedPostIds = ref(new Set()); // Set สำหรับเก็บ post ids ที่ถูกรันด้อมแล้ว
  const randomPageNumber = ref(1);
  const endPageNumber = ref(false);

  async function random() {
    try {
      const unrandomizedPosts = postStore.posts.filter(post => 
        !randomizedPostIds.value.has(post.id)); // Only include posts that haven't been randomized/tagged
    
      if (unrandomizedPosts.length > 1) {
        const index = Math.floor(Math.random() * unrandomizedPosts.length);
        randomPageNumber.value = unrandomizedPosts[index].id;
        await postStore.getPostById(randomPageNumber.value);
        // Do not immediately add to randomizedPostIds here
      } else if (unrandomizedPosts.length === 1) {
        randomPageNumber.value = unrandomizedPosts[0].id;
        await postStore.getPostById(randomPageNumber.value);
        // Still, do not add here as tagging is not yet confirmed
      } else {
        endPageNumber.value = true;
        router.push('/soldout');
        return;
      }
    } catch (error) {
      console.error('Error in random function:', error);
    }
  }
  

  function resetRandomization() {
    randomizedPostIds.value.clear();
  }

  return { randomPageNumber, random, resetRandomization ,endPageNumber};
});
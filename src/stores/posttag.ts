import { ref } from 'vue'
import { defineStore } from 'pinia'
import type { Posttag } from '@/types/Posttag';
import posttagService from '@/service/posttag';

export const usePosttagStore = defineStore("posttag", () => {
  const posttags = ref<Posttag[]>([]);
  const posttag = ref<Posttag>();
  
 

  async function getPosttags() {
    try {
      const response = await posttagService.getPosttags() 
      posttags.value = response.data; 
    } catch (error) {
      console.error('Error fetching posttags:', error);
    }
  }



async function searchTextByUserId(searchText: string, userId: number) {
  try {
      const response = await posttagService.searchTextByUserId(searchText , userId);
      posttags.value = response.data; 
  } catch (error) {
      console.error('Error fetching posttags:', error);
  }
}


  async function getPostTagByUserId(userId: number) {
    try {
      const response = await posttagService.getPostTagByUserId(userId)
      posttags.value = response.data;
    } catch (error) {
      console.error(`Error fetching posttag with id ${userId}:`, error);
      throw error;
    }
  }
  

  async function getPostTagById(id: number) {
    try {
      const response = await posttagService.getPostTagById(id);
      posttag.value = response.data;
    } catch (error) {
      console.error(`Error fetching posttag with id ${id}:`, error);
      throw error;
    }
  }
  

  async function updateTag(id:number, idTag:number) {
    try {
      const res = await posttagService.getTag(id);
      const updateTag = res.data;
      updateTag.tag!.id = idTag;
      const result = await posttagService.updateTag(id, updateTag);
      posttag.value = result.data;
    } catch (error) {
      console.error(`Error fetching posttag with id ${id}:`, error);
      throw error;
    }
  }

  return { posttags, getPosttags, getPostTagById,getPostTagByUserId , updateTag, searchTextByUserId,posttag};
});


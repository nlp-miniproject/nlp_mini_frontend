import { ref } from "vue";
import { defineStore } from "pinia";
import auth from "@/service/auth";
import router from "@/router";
import { useUserStore } from "./user";
import type { User } from '@/types/User';

export const useAuthStore = defineStore("auth", () => {
  const authName = ref({});
  const userStore = useUserStore();
  const isLogin = ref(false);

  const login = async (email: string, password: string) => {
    try {
      const response = await auth.login(email, password);
  
      localStorage.setItem("access_token", response.data.access_token);
  
      localStorage.setItem("user", JSON.stringify(response.data.user));
  
      if (response) {
        const user__: User = {
          id: response.data.user.id,
          username: response.data.user.username,
          email: response.data.user.email,
          password: password,
          role: response.data.user.role,
        };
  
        userStore.setUser(user__);  
        if (userStore.currentUser.role === 'volunteer') {
          router.push("/diarys");
        } else if (userStore.currentUser.role === 'admin') {
          router.push("/report-mantal");
        } else {
          router.push("/");
        }
      } else {
        console.error("User does not have an account");
        return null;
      }
      
      authName.value = JSON.parse(localStorage.getItem("user") || "{}");
    } catch (error) {
      console.log("Error during login:", error);
    }
  
    isLogin.value = true;
  };
  

  const logout = () => {
    localStorage.removeItem("access_token");
    localStorage.removeItem("user");

    authName.value = "";
    const currentUser = ref<User>({
      id: -1,
      username: "",
      email: "",
      password: "",
      role:"",
    });
    userStore.setUser(currentUser.value);
    router.replace("/login");
  };

  const register = async (
    email: string,
    password: string,
    username: string
  ) => {
    try {
      const response = await auth.authenticate(email, password, username);

      if (response.status === 409) {
        return response;
      }
      if (response.data != null) {
        return true;
      }
    } catch (error) {
      console.log(error);
      return null;
    }
  };

  const registervol = async (
    email: string,
    password: string,
    username: string
  ) => {
    try {
      const response = await auth.authenticatevol(email, password, username);

      if (response.status === 409) {
        return response;
      }
      if (response.data != null) {
        return true;
      }
    } catch (error) {
      console.log(error);
      return null;
    }
  };

  const registerres = async (
    email: string,
    password: string,
    username: string
  ) => {
    try {
      const response = await auth.authenticateres(email, password, username);

      if (response.status === 409) {
        return response;
      }
      if (response.data != null) {
        return true;
      }
    } catch (error) {
      console.log(error);
      return null;
    }
  };

  const getUserFromLocalStorage = () => {
    const userString = localStorage.getItem("user");
    if (userString) {
      try {
        const userObject: User = JSON.parse(userString);
        userStore.setUser(userObject);
        authName.value = userObject;
        isLogin.value = true;
      } catch (e) {
        console.error("Failed to parse user from localStorage:", e);
        isLogin.value = false;
      }
    } else {
      console.log("No user found in localStorage.");
      isLogin.value = false;
    }
  };

  return {
    login,
    logout,
    register,
    getUserFromLocalStorage,
    registerres,
    registervol
  };
});

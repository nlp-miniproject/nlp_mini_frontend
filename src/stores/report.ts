import { ref } from 'vue';
import { defineStore } from 'pinia';
import { useRouter } from 'vue-router'; 
import reportService from "@/service/report";
import type { Posttag } from '@/types/Posttag';

interface RoleCount {
  user_role: string;
  count: number;
}

export const useReportStore = defineStore("tag", () => {
    const postTags = ref<Posttag[]>([]);
    const countRoles = ref<RoleCount[]>([]);
    const userList = ref<{ user_id: number; user_username: string; postCount: number }[]>([]);
    const uniqueTaggedPostsCount = ref<number | null>(null); 
    const router = useRouter();
    const reportPost = async (reportData: { user_id: number, post_id: number, reason: string }) => {
        try {
            const response = await reportService.ReportPost(reportData.reason, reportData.post_id, reportData.user_id);
            console.log('Report submitted successfully:', response.data);
            return response.data;
        } catch (error) {
            console.error('Error submitting report:', error);
            throw error;
        }
    }
    
    const getDailyReport = async () => {
        try {
            const res = await reportService.countUserRoles();
            if (res.data) {
                countRoles.value = res.data.map((role: { user_role: any; count: any; }) => ({
                    user_role: role.user_role,
                    count: role.count
                }));
            }
        } catch (error: any) {
            if (error.response && error.response.status === 403) {
                router.push('/forbidding');  
            } else {
                console.error(error);
            }
        }
    };
    const getVolunteer = async () => {
        try {
            const res = await reportService.getVolunteerPostCounts();
            if (res.data) {
                userList.value = res.data;
                console.log(userList.value);
            }
        } catch (error: any) {
            if (error.response && error.response.status === 403) {
                router.push('/forbidding'); 
            } else {
                console.error(error);
            }
        }
    };

    const getPostTag = async () => {
        try {
            const res = await reportService.getPostTag();
            if (res.data) {
                postTags.value = res.data;
                console.log(postTags.value);
            }
        } catch (error: any) {
            if (error.response && error.response.status === 403) {
                router.push('/forbidding'); 
            } else {
                console.error(error);
            }
        }
    };
    const fetchUniqueTaggedPostsCount = async () => {
        try {
            const res = await reportService.fetchUniqueTaggedPostsCount();
            uniqueTaggedPostsCount.value = res.data;  
        } catch (error: any) {
            if (error.response && error.response.status === 403) {
                router.push('/forbinding'); 
            } else {
                console.error(error);
            }
        }
    };

    return { 
        getDailyReport, 
        getVolunteer, 
        countRoles, 
        userList, 
        getPostTag, 
        postTags,
        uniqueTaggedPostsCount, 
        fetchUniqueTaggedPostsCount ,
        reportPost
    };
});
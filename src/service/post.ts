import http from "./axios";

function postDiary(txtContent : string , source : string , datetime : string , user_id : number) {
    const token = localStorage.getItem("access_token");
    const config = token ? { headers: { Authorization: `Bearer ${token}` } } : {};
    return http.post("/posts", {txtContent,source,datetime,user_id},config)
}
  
function patchDiary(id: number ,txtContent : string) {
    const token = localStorage.getItem("access_token");
    const config = token ? { headers: { Authorization: `Bearer ${token}` } } : {};
    return http.patch(`/posts/${id}`, {txtContent},config)
}

function getPostAll() {
    const token = localStorage.getItem("access_token");
    const config = token ? { headers: { Authorization: `Bearer ${token}` } } : {};
    
    return http.get("/posts", config)
}

function getPosts(user_id: number) {
    const token = localStorage.getItem("access_token");
    const config = token ? { headers: { Authorization: `Bearer ${token}` } } : {};
  
    return http.get(`/posts/exclude-tags/${user_id}`, config);
  }
  

function getPostById(id : number) {
    const token = localStorage.getItem("access_token");
    const config = token ? { headers: { Authorization: `Bearer ${token}` } } : {};
    return http.get(`/posts/${id}`, config)
}

function getPostBySource(user_id: number, searchText?: string, searchDate?: string | null) {
    let url = `/posts/by-source/${user_id}`;
    const token = localStorage.getItem("access_token");
    const config = token ? { headers: { Authorization: `Bearer ${token}` } } : {};
    const queryParams = new URLSearchParams();
    if (searchText) {
        queryParams.append('searchText', searchText);
      }
      if (searchDate) {
        queryParams.append('searchDate', searchDate);
      }
      if (queryParams.toString()) {
        url += `?${queryParams.toString()}`;
      }
    return http.get(url,config)
}

function getPostBySourceId(user_id: number ,id : number) {
    const token = localStorage.getItem("access_token");
    const config = token ? { headers: { Authorization: `Bearer ${token}` } } : {};
    return http.get(`/posts/by-source/${user_id}/${id}`,config)
}

function getPostBySearch(searchText: string) {
    const token = localStorage.getItem("access_token");
    const config = token ? { headers: { Authorization: `Bearer ${token}` } } : {};
    return http.get(`/posts/searchAll?searchText=${searchText}`,config)
}

function deletePostById(id: number) {
    const token = localStorage.getItem("access_token");
    const config = token ? { headers: { Authorization: `Bearer ${token}` } } : {};
    return http.delete(`/posts/${id}`,config)
}

export default { postDiary , patchDiary , getPostAll , getPosts , getPostById , getPostBySource , getPostBySourceId , getPostBySearch , deletePostById}
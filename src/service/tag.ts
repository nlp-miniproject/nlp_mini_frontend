import http from "./axios";

function getTags() {
    return http.get("/posts")
}

function getTagById(id : number) {
    return http.get(`/tags/${id}`)
}

export default {getTags , getTagById}
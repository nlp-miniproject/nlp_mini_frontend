import http from "./axios";

function countUserRoles() {
  const token = localStorage.getItem("access_token");
  const config = token ? { headers: { Authorization: `Bearer ${token}` } } : {};
  return http.get(`/users/count-roles`, config)
}
function getVolunteerPostCounts() {
  const token = localStorage.getItem("access_token");
  const config = token ? { headers: { Authorization: `Bearer ${token}` } } : {};
    return http.get(`/users/volunteer-post-counts`,config)
  }
  function getPostTag() {
  const token = localStorage.getItem("access_token");
  const config = token ? { headers: { Authorization: `Bearer ${token}` } } : {};
    return http.get(`/post-tags`,config)
  }
  function fetchUniqueTaggedPostsCount() {
    const token = localStorage.getItem("access_token");
    const config = token ? { headers: { Authorization: `Bearer ${token}` } } : {};
    return http.get('/post-tags/count-unique-tagged-posts', config)
  }
  function ReportPost(reason:string, post_id:number ,user_id:number){

    const reportData = {
      user_id: user_id,   
      post_id: post_id, 
      reason: reason      
    };
    return http.post('/report-post', reportData)
    .then(response => {
      console.log('Report submitted successfully:', response.data);
      return response.data;
    })
    .catch(error => {
      console.error('Error submitting report:', error);
      throw error;
    });
  }

export default { countUserRoles , getVolunteerPostCounts,getPostTag,fetchUniqueTaggedPostsCount,ReportPost}
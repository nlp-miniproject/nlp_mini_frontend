import http from "./axios";

const authenticate = async (email: string, password: string, username: string) => {
  return await http.post('/auth/register', {
    email,
    username: username,
    password: password
  })
}

const authenticatevol = async (email: string, password: string, username: string) => {
  return await http.post('/auth/register-Volunteer', {
    email,
    username: username,
    password: password
  })
}

const authenticateres = async (email: string, password: string, username: string) => {
  return await http.post('/auth/register-Researcher', {
    email,
    username: username,
    password: password
  })
}


function login(email: string, password: string) {
    return http.post("/auth/login", { email, password });
  }  

export default { login, authenticate, authenticatevol, authenticateres}

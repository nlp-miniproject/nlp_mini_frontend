import http from "./axios";

function posttags(timestamp : Date ,user: number, post: number , tag: number) {
    return http.post("/post-tags", {timestamp ,user, post , tag });
  }  

function getPosttags() {
  return http.get("/post-tags")
}

function searchTextByUserId(searchText: string, userId: number) {
    const url = `/post-tags/search?searchText=${searchText}&userId=${userId}`;
    return http.get(url)
}

function getPostTagByUserId(userId: number) {
  const token = localStorage.getItem("access_token");
  const config = token ? { headers: { Authorization: `Bearer ${token}` } } : {};
  const url = `/post-tags/user/${userId}`;
  return http.get(url,config)
}

function getPostTagById(id: number) {
  return http.get(`/post-tags/${id}`)
}

function getTag(id:number) {
  return http.get(`/post-tags/${id}`)
}

function updateTag(id:number , updateTag: any) {
  return http.patch(`/post-tags/${id}` , updateTag)
}

export default { posttags , getPosttags , searchTextByUserId , getPostTagByUserId , getPostTagById , getTag , updateTag}
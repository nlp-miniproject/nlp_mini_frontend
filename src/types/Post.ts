import type { Posttag } from "./Posttag";

export interface Post {
    id: number;
    title: string;
    txtContent: string;
    source: string;
    datetime: string;
    postTags?: Posttag[];
  }
  
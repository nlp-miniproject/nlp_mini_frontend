import type { Post } from "./Post";
import type { Tag } from "./Tag";
import type { User } from "./User";

export interface Posttag {
    id: number;
    timestamp: Date;
    user?: User;
    post?: Post; 
    tag?: Tag;
  }
  
import type { Posttag } from "./Posttag";

export interface Tag {
  id: number;
  tagName: string;  
  postTags?: Posttag;
}
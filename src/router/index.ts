import { createRouter, createWebHistory } from "vue-router";

import LoginView from "@/views/LoginView.vue";
import PostView from "@/views/PostView.vue";
import { ref } from "vue";
import SeeAll from "@/components/SeeAll.vue";
import HistoryTag from "@/components/HistoryTag.vue";
import HistoryTagingDetail from "@/components/HistoryTagingDetail.vue";
import SoldOut from "@/components/SoldOut.vue";
import DiaryView from "@/views/diaryViews/DiaryView.vue";
import HisDiaryView from "@/views/diaryViews/HisDiaryView.vue";
import RegisterVolView from "@/views/RegisterVolView.vue";
import RegisterResView from "@/views/RegisterResView.vue";
import RegisterView from "@/views/ResgisterView.vue";
import PreViewLogin from "@/views/PreViewLogin.vue";
import ReportMantal from "@/views/report/adminView.vue"
import PostManagement from "@/views/report/PostManagementView.vue";
import { useRandomStore } from "@/stores/random";
import type { User } from "@/types/User";
import ForbiddingView from "@/views/ForbiddingView.vue";

const userData = localStorage.getItem("user");
const user = ref<User | null>(userData ? JSON.parse(userData) : null);
const hashVolunteer = simpleHash("volunteer");
const hashPersonnel = simpleHash("personnel");
const hashResearcher = simpleHash("researcher");

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: "/post",
      name: "post",
      components: {
        default: PostView,
        menu: () => import("@/components/menus/MainMenu.vue"),
      },
      meta: {
        layout: "FullLayout",
        requiresAuth: true,
      },
    },
    {
      path: "/prelogin",
      name: "preview",
      components: {
        default: PreViewLogin,
      },
      meta: {
        layout: "FullLayout",
        requiresAuth: false,
      },
    },
    {
      path: "/login",
      name: "login",
      components: {
        default: LoginView,
      },
      meta: {
        layout: "FullLayout",
        requiresAuth: false,
      },
    },
    {
      path: "/forbidding",
      name: "forbidding",
      components: {
        default: ForbiddingView,
        menu: () => import("@/components/headers/MainHeader.vue"),
      },
      meta: {
        layout: "FullLayout",
        requiresAuth: true,
      },
    },

    /// register/personnel
    {
      path: `/register/${hashPersonnel}`,
      name: "registerPersonnel",
      component: RegisterView,
      meta: {
        layout: "FullLayout",
        requiresAuth: false,
      },
    },
    {
      path: "/register/personnel",
      redirect: `/register/${hashPersonnel}`,
    },

    /// register/volunteer
    {
      path: `/register/${hashVolunteer}`,
      name: "registervol",
      component: RegisterVolView,
      meta: {
        layout: "FullLayout",
        requiresAuth: false,
      },
    },
    {
      path: "/register/volunteer",
      redirect: `/register/${hashVolunteer}`,
    },

    /// register/researcher
    {
      path: `/register/${hashResearcher}`,
      name: "registerres",
      component: RegisterResView,
      meta: {
        layout: "FullLayout",
        requiresAuth: false,
      },
    },
    {
      path: "/register/researcher",
      redirect: `/register/${hashResearcher}`,
    },
    {
      path: '/',
      component: SeeAll,
      meta: {
        layout: "FullLayout",
        requiresAuth: true,
      },
      beforeEnter: (to, from, next) => {
        const token = localStorage.getItem("access_token");
        const user = JSON.parse(localStorage.getItem("user") || "{}");
    
        if (token && user) {
          if (user.role === 'volunteer') {
            next('/diarys');
          } else if (user.role === 'admin') {
            next('/report-mantal');
          } else if (user.role === 'personnel' || user.role === 'researcher') {
            next(); 
          } else {
            next('/forbidding'); 
          }
        } else {
          next('/prelogin');
        }
      },
    },
    
    
    {
      path: "/seeAll/:id",
      name: "seeAll",
      components: {
        default: SeeAll,
        menu: () => import("@/components/headers/MainHeader.vue"),
      },
      meta: {
        layout: "Mainlayout",
        requiresAuth: true,
      },
    },
    {
      path: "/hisTag",
      name: "hisTag",
      components: {
        default: HistoryTag,
        menu: () => import("@/components/headers/MainHeader.vue"),
      },
      meta: {
        layout: "FullLayout",
        requiresAuth: true,
      },
    },
    {
      path: "/hisTagDe/:id",
      name: "hisTagDe",
      components: {
        default: HistoryTagingDetail,
        menu: () => import("@/components/headers/MainHeader.vue"),
      },
      meta: {
        layout: "FullLayout",
        requiresAuth: true,
      },
    },
    {
      path: "/soldout",
      name: "soldout",
      components: {
        default: SoldOut,
        menu: () => import("@/components/headers/MainHeader.vue"),
      },
      meta: {
        layout: "FullLayout",
        requiresAuth: true,
      },
    },
    {
      path: "/diarys",
      name: "diarys",
      components: {
        default: DiaryView,
        menu: () => import("@/components/headers/MainHeader.vue"),
      },
      meta: {
        layout: "FullLayout",
        requiresAuth: true,
      },
    },
    {
      path: "/diary/:id",
      name: "diary",
      components: {
        default: DiaryView,
        menu: () => import("@/components/headers/MainHeader.vue"),
      },
      meta: {
        layout: "FullLayout",
        requiresAuth: true,
      },
    },
    {
      path: "/report-mantal",
      name: "report-mantal",
      components: {
        default: ReportMantal,
        menu: () => import("@/components/headers/MainHeader.vue"),
      },
      meta: {
        layout: "FullLayout",
        requiresAuth: true,
      },
    },
    {
      path: "/post-management",
      name: "post-management",
      components: {
        default: PostManagement,
        menu: () => import("@/components/headers/MainHeader.vue"),
      },
      meta: {
        layout: "FullLayout",
        requiresAuth: true,
      },
    },
    {
      path: "/hisdiary",
      name: "hisdiary",
      components: {
        default: HisDiaryView,
        menu: () => import("@/components/headers/MainHeader.vue"),
      },
      meta: {
        layout: "FullLayout",
        requiresAuth: true,
      },
    },
  ],
});

function simpleHash(str: string) {
  let hash = 0;
  for (let i = 0; i < str.length; i++) {
    hash += str.charCodeAt(i);
  }
  return hash;
}

function isLogin() {
  return !!localStorage.getItem("access_token");
}

router.beforeEach(async (to, from, next) => {

  if (to.meta.requiresAuth && !isLogin()) {
    console.warn('[Vue Router Warn] User not authenticated, redirecting to /prelogin');
    next({
      path: '/prelogin',
      query: { redirect: to.fullPath },
    });
    return; 
  }





  if (to.path === '/') {
    const user = JSON.parse(localStorage.getItem("user") || "{}");
    if (user && (user.role === 'personnel' || user.role === 'researcher')) {
    const randomStore = useRandomStore();
    await randomStore.random(); 
    const pageRandom = randomStore.randomPageNumber;

    if (pageRandom != null) {
      next(`/seeAll/${pageRandom}`);
    } 
  }
  else if (user.role === 'volunteer') {
    next('/diarys');
  } else if (user.role === 'admin') {
    next('/report-mantal');
  }
    return; 
  }


  next();
});




export default router;

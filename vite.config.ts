import { fileURLToPath, URL } from 'node:url'
import { defineConfig } from 'vite'
import vue from '@vitejs/plugin-vue'
import vueJsx from '@vitejs/plugin-vue-jsx'

// https://vitejs.dev/config/
export default defineConfig({
  plugins: [
    vue(),
    vueJsx(),
  ],
  resolve: {
    alias: {
      '@': fileURLToPath(new URL('./src', import.meta.url))
    }
  },
  build: {
    // ตั้งค่าแบ่งโค้ดเป็นส่วนๆ เพื่อไม่ให้ Chunk ขนาดใหญ่เกินไป
    rollupOptions: {
      output: {
        manualChunks: {
          // แยก library ออกเป็น chunk ใหม่ (สามารถปรับตามความเหมาะสมของโปรเจ็กต์)
          vendor: ['vue', 'axios'], // ตัวอย่างการแยก library 
        },
      },
    },
    // เพิ่มขีดจำกัดคำเตือนของขนาด Chunk
    chunkSizeWarningLimit: 4000, // ปรับขีดจำกัดคำเตือนเป็น 1000 kB (หรือปรับได้ตามต้องการ)
    minify: 'terser', // เปิดใช้ minification ด้วย terser เพื่อลดขนาดไฟล์
  }
})
